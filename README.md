# Downloadify
### Downloadify, the download library is highly configurable and customizable library to download files from server. With this library downloads can be configured with a number of constraints & download rules which can be customized to any extend and can be schedule the download at a specific time .

---

To use it:

* Get an instance of DownloadManager and can call appropriate api of DownloadManager   for createAndStart/start/restart(with new params)/pause/resume/stop/delete downloads or get all   downloads data with getAlldownloads(Context context) method.

* Listen for broadcasts of type i) IntentKeys.BROADCAST_DOWNLOAD_PROGRESS

  ii) IntentKeys.BROADCAST_DOWNLOAD_STATUS

  iii) IntentKeys.BROADCAST_DOWNLOAD_EXCEPTION **to get the status of the downloads**.

**Downloadify** supports high customization and can be configured in different ways:

 DownloadManager class handles all the controls of a download. A Download task can be created by using the api  createAndStartDownload() of the DownloadManager class.

```css
 /**
     *  This method creates a @link {DownloadTask} instance which holds all the  datas necessarry for a download
     * @param context
     * @param downloadUrl  the url from which the file to be downloaded
     * @param queryparameters  optional queryparameters which should be appended with @param downloadUrl
     * @param resourceId       An identifier string used to represent this file/asset
     * @param downloader       An instance of downloader logic which holds the logic for downloading the file
     * @param downloadRules    Optional download rules specifying the networks that could be used/ times at which download should happen
     * @param constraintChecksList  list of instances of constraint check which checks certain conditions before a download happens eg: @link {BatteryConstraintChecker}
     */

```

```js
DownloadManager.getInstance().createAndStartDownload(Context context, URL downloadUrl, HashMap<String, String> queryparameters, String resourceId, Downloader downloader, DownloadRules downloadRules, List<ConstraintChecks> constraintChecksList);
```

**Downloader** :  The sample has two basic custom classes (SimpleDownloader.java, SimpleInternalStorageDownloader.java) that handles the Downloader logic. Users can pass any class of type Downloader to include custom logic to download the file for a task.

**DownloadRules** : DownloadRules specify the time window and networks through which files can be downloaded.

**constraintChecksList**  :  List of ConstraintChecks can be passed for a download task which will be validated before a download starts/resumes. ConstraintChecks  performs a validation through method checkIfConstrained() and can notify whether the validation have passed or not through ConstraintCallbacks. User can add any number of validation actions in constraintChecksList. An example of constraint check present in library is BatteryConstraintChecker.java which check if battery has enough power to carry out a download  & SimpleNetworkConstraintCheck.java  which check if there is an internet connection

**resourceId** :  each download task is identified through this resource id and manipulation of a download is referenced through this id. 






---